# CAR360 Display Sample App #

A sample app showing how to use the CAR360 display framework

### How do I get set up? ###

* Copy "Car360DisplayFramework.framework" into the root folder
* Open "DisplaySDKSample.xcodeproj"
* Run the app

### Who do I talk to? ###

* [bruno@egosventures.com](mailto:bruno@egosventures.com)
* [remy@egosventures.com](mailto:remy@egosventures.com)
* [Car360.net](http://car360.net)
* [Car360 iOS framework documentation](doc.car360app.com/iOS/display)