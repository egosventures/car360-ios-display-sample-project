//
//  Car360ViewController.swift
//  DisplaySDKSample
//
//  Created by Remy Cilia on 9/26/16.
//  Copyright © 2016 Egos Ventures. All rights reserved.
//

import UIKit
import Car360DisplayFramework

class Car360ViewController: UIViewController {
    
    @IBOutlet weak var car360Viewer: OnlineViewer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let carId = "4e24e73c977eaaa1c1a6e2cc5a95d837"
        
        let progressBlock = {(completedRatio: Double) -> Void in
            print("progress: \(completedRatio)")
        }
        
        let completionBlock = {(error: OnlineViewerError?) -> Void in
            if error == nil || error == OnlineViewerError.None {
                print("completed loading with no error")
            } else {
                print("error: \(error.debugDescription)")
            }
        }
        
        Car360DisplayFramework.config.resolution = .Res640x360
     
        car360Viewer.loadSpin(carId, progressBlock: progressBlock, completionBlock: completionBlock)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
