//
//  ViewController.swift
//  DisplaySDKSample
//
//  Created by Remy Cilia on 9/26/16.
//  Copyright © 2016 Egos Ventures. All rights reserved.
//

import UIKit
import Car360DisplayFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func openViewControllerAction(_ sender: AnyObject) {
        let viewerVC = Car360DisplayFramework.getOnlineViewerViewControllerForSpin(carId: "f2b1404f3b5e97fb1026ff6121c5d914") { (config) -> (Void) in
            config.resolution = .Res960x540
        }
        
        if let viewerVC = viewerVC {
            viewerVC.progressBlock = {(completedRatio: Double) -> Void in
                print("progress: \(completedRatio)")
            }
            
            viewerVC.completionBlock = {(error: OnlineViewerError?) -> Void in
                if error == nil || error == OnlineViewerError.None {
                    print("completed loading with no error")
                } else {
                    print("error: \(error.debugDescription)")
                    switch error! {
                    case .NotFound:
                        print("not found")
                    case .NetworkUnavailable:
                        print("network unavailable")
                    case .IO:
                        print("i/o")
                    case .None:
                        print("none")
                    }
                    
                }
            }
            
            self.present(viewerVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func clearCacheAction(_ sender: AnyObject) {
        Car360DisplayFramework.clearCache()
    }
    
    @IBAction func unwindToViewController(_ segue: UIStoryboardSegue) {}
}

